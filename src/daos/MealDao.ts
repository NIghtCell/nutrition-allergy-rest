import { createConnection } from "typeorm";
import { Meal } from "../entity/Meal";
import { Food } from "../entity/Food";
import { User } from "../entity/User";
import { Heaviness } from "../../Heaviness";

export class MealDao {
  static async getAll(): Promise<Meal[]> {
    return await createConnection().then(async connection => {
      return await connection.manager
        .find(Meal)
        .finally(() => connection.close());
    });
  }

  static insertMeal(
    user: User,
    food: Food[],
    date: Date,
    heaviness: Heaviness
  ) {
    createConnection()
      .then(async connection => {
        console.log("Inserting a new meal into the database");
        const meal = new Meal();
        meal.date = date;
        meal.food = food;
        meal.heaviness = heaviness;
        meal.user = user;

        await connection.manager.save(meal);
        console.log("Inserted meal into the database");
        await connection.close();
      })
      .catch(error => console.log(error));
  }

  static async getAllMealsByDate(date: Date): Promise<Meal[]> {
    return await createConnection().then(async connection => {
      return await connection
        .getRepository(Meal)
        .find({ date: date })
        .finally(() => connection.close());
    });
  }
}
