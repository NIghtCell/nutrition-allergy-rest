import { Heaviness } from "../../Heaviness";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinTable,
  ManyToMany,
  ManyToOne
} from "typeorm";
import { User } from "./User";
import { Food } from "./Food";

@Entity()
export class Meal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: Date })
  date: Date;

  @ManyToMany(
    type => Food,
    food => food.meals,
    {
      eager: true
    }
  )
  @JoinTable()
  food: Food[];

  @Column()
  heaviness: Heaviness;

  @ManyToOne(
    type => User,
    user => user.meals,
    {
      eager: true
    }
  )
  user: User;
}
