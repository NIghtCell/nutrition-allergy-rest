import { MealDao } from "./daos/MealDao";
const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());

app.get("/nutrition-allergy/meals", (req, res) => {
  MealDao.getAll().then(meals => {
    console.log(meals);
    res.send(meals);
  });
});

const port = 4000;

app.listen(port, () => console.log(`Listening on port ${port}...`));
